#!/bin/bash
# Automate git tagging and pypi upload of code

usage() {
    echo "Usage: $(basename $0) <version>"
    echo "       where <version> is the new relasese version you want to create"
}

# Branch from which we expect to tag and push the new release
master="master"  

# Checks
[[ ! -z $1 ]] && version=$1
[[ -z $(which twine) ]] && echo "error: twine is required" && exit 1
[[ -z $(which wheel) ]] && echo "error: wheel is required" && exit 1
[[ -z $(which gitcl) ]] && echo "warning: gitcl is not installed, no changelog handling" && sleep 3
[[ -z $(which gitcl) ]] && [[ -z $version ]] && usage && exit 1
[ ! -f pyproject.toml ] && [[ -z $(grep Extension setup.py) ]] && target="bdist_wheel --universal" || target="sdist"
[ -f pyproject.toml ] && [[ -z $(pip freeze | grep build) ]] && echo "error: build is required" && exit 1

# Make sure we are on master
git checkout $master || exit 1
echo

# Get new version
if [[ -z $version ]] ; then
    if [[ ! -z $(which gitcl) ]] ; then
	#gitcl
	version=$(gitcl --guess-version)
	echo
	read -p "Guessing new version is $version, ok? [y/Y] " -n 1 -r
	[[ ! $REPLY =~ ^[Yy]$ ]] && exit 0    
    else
	echo "Please provide version"
	exit 1
    fi
fi

# Make sure the repo is clean
[[ ! -z $(git status --porcelain|grep -v '??') ]] && echo "error: uncommited changes (check with git status)" && exit 1

# Update CHANGELOG if present
if [[ ! -z $(which gitcl) ]] ; then
    if [ -f CHANGELOG.md ] ; then
	mv CHANGELOG.md CHANGELOG.md.tmp
	gitcl --version $version | sed "s/HEAD/$version/g" > CHANGELOG.md
	cat CHANGELOG.md.tmp >> CHANGELOG.md
	rm -f CHANGELOG.md.tmp
	$EDITOR CHANGELOG.md
	[ $? != 0 ] && exit 1
	file_changelog=CHANGELOG.md
    fi
fi

echo
read -p "Ready to bump new release $version? [y/Y] " -n 1 -r
[[ ! $REPLY =~ ^[Yy]$ ]] && exit 0

# Update version
file_version=$(find . -name "_version.py" | grep -v build | grep -v tox | grep -v env)

if [[ -f $file_version ]] ; then
    echo "__version__ = '"$version"'" > $file_version
    git commit -m "Bump version $version" $file_version $file_changelog
else
    # TODO: check that version in setup.py is handled correctly
    [ -f pyproject.toml ] && config_file=pyproject.toml || config_file=setup.py
    sed -i "s/\(version\s*=\s*['\"]\)\([0-9]\+.[0-9]\+\.[0-9]\+\)\(['\"]\)/\\1$version\\3/" $config_file
    git commit -m "Bump version $version" $config_file $file_changelog
fi

echo
read -p "Ready to push to pypi? [y/Y] " -n 1 -r
[[ ! $REPLY =~ ^[Yy]$ ]] && exit 0

# Tag release and push to pypi
git tag -a -m "$version" $version && \
    git checkout $version  && \
    rm -rf build/* dist/*

[ $? != 0 ] && echo error && exit 1

if [ -f pyproject.toml ] ; then
    python -m build
else
    python setup.py $target
fi

[ $? == 0 ] && \
    twine upload dist/*  && \
    git push origin $master $version && \
    git checkout $master
